# k8s-wireguard

Run a Wireguard server/client on Kubernetes.

## Requirements

- Loaded wireguard module (see https://gitlab.com/ydkn/k8s-wireguard-module)

## Installation

```bash
kubectl apply -f https://gitlab.com/ydkn/k8s-wireguard/raw/master/deployment.yaml
```
