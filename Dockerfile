# base image
FROM alpine:3

# args
ARG VCS_REF
ARG BUILD_DATE

# labels
LABEL maintainer="Florian Schwab <me@ydkn.io>" \
  org.label-schema.schema-version="1.0" \
  org.label-schema.name="ydkn/k8s-wireguard" \
  org.label-schema.description="Wireguard server/client for Kubernetes" \
  org.label-schema.version="0.1" \
  org.label-schema.url="https://hub.docker.com/r/ydkn/k8s-wireguard" \
  org.label-schema.vcs-url="https://gitlab.com/ydkn/k8s-wireguard" \
  org.label-schema.vcs-ref=$VCS_REF \
  org.label-schema.build-date=$BUILD_DATE

# install dependencies
RUN apk add --no-progress --no-cache wireguard-tools iptables

# add run script
COPY run.sh /run.sh

VOLUME [ "/etc/wireguard" ]

# default command
CMD ["/run.sh"]
