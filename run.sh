#!/bin/sh -e

# Bring all interfaces up
for conf in /etc/wireguard/*.conf; do
  IFACE=$(basename "${conf}" .conf)

  # Bring interface up
  echo "Starting Wireguard interface ${IFACE}"
  wg-quick up "${IFACE}"
done

# Handle shutdown
finish() {
  for conf in /etc/wireguard/*.conf; do
    IFACE=$(basename "${conf}" .conf)

    # Bring interface down
    echo "Shutting down Wireguard interface ${IFACE}"
    wg-quick down "${IFACE}" || true
  done

  exit 0
}

trap finish SIGTERM SIGINT SIGQUIT

sleep infinity &
wait $!
